<!DOCTYPE HTML>
<html>
	<head>
		<?php include "./includes/head.php";?>
		<title>  Hardware  | Rob Van Keilegom</title>
	</head>
<body>
	
<header>
	<?php include "./includes/header.php";?>
</header>

<nav id="nav" role="navigation">
	<?php include "./includes/navigatie.php";?>
</nav>	

<div class="main">
<div class="content">
<h1>Website</h1>
  <p>De website bestaat uit 2 velden. 1 veld waar je kan zien waar je hebt geschoten, dit veld is in principe het zelfde als het veld dat je ziet op het LED bord, op het andere bord zie je waar je boten staan. De boten worden automatisch gegenereerd door op de reset knop te duwen. De website word volledig op de server gegenereerd dmv php code. Door ergens in het veld te klikken gaat de server kijken of er al een boor op die plek staat. als er een staat word er na gekeken hoeveel keer er op de boot kan geschoten worden. Er wordt 1 van het aantal afgetrokken en als het getal 0 is wil dat zeggen dat er niets meer overschiet van de boot, en zinkt de boot. De boot krijgt dan een andere kleur op het speelveld. Al de data over de schoten wordt bijgehouden in een database. Als de data word dus bijgehouden ook als de connectie naar de server even wegvalt. De borden kunnen enkel gewist worden door op de reset knop te duwen.</p>
<h1>Foto's</h1>
      <a href="images/slides/website.png" class="fancybox" title="Website"><img src="images/slides/website.png" alt="Website" /></a>
    
      <a href="images/slides/websitecode.png" class="fancybox" title="PHP Code Website"><img src="images/slides/websitecode.png" alt="PHP Code Website" /></a>
    
      <a href="images/slides/phpmyadmin.png" class="fancybox" title="PHPMyAdmin"><img src="images/slides/phpmyadmin.png" alt="PHPMyAdmin" /></a>
    
</div>
</div>
<?php 
include "./includes/footer.php";
?>
    <script>
        $(document).ready(function() {
            $('.fancybox').fancybox();
        });
    </script>
</body>
</html>   