<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
<meta name="viewport" content="initial-scale=1">
<script src="js/jquery-2.1.4.js"></script>
<script src="js/nav.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>

<!-- stylesheets -->
<link rel="stylesheet" type="text/css" href="./stylesheets/reset.css" />
<link rel="stylesheet" type="text/css" href="./stylesheets/mainstyle.css" />
<link rel="stylesheet" type="text/css" href="./stylesheets/mainstyle_css3.css" />
<link rel="stylesheet" media="screen and (min-device-width: 800px)" type="text/css" href="./stylesheets/desktop.css" />
<link rel="stylesheet" media="screen and (min-device-width: 800px)" type="text/css" href="./stylesheets/desktop_css3.css" />
<link rel="stylesheet" media="screen and (max-device-width: 800px)" href="./stylesheets/mobile.css" />
<link rel="stylesheet" media="screen and (max-device-width: 800px)" href="./stylesheets/mobile_css3.css" />

<!-- fancybox -->
<link rel="stylesheet" href="stylesheets/jquery.fancybox.css" type="text/css" media="screen" />
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css' /> <!-- headfont -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /><!-- navfont -->


<!-- FAVICON SHIT -->

<link rel="apple-touch-icon" sizes="57x57" href="./images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="./images/favicon//apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="./images/favicon//apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="./images/favicon//apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="./images/favicon//apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="./images/favicon//apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="./images/favicon//apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="./images/favicon//apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="./images/favicon//apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="./images/favicon//android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="./images/favicon//favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="./images/favicon//favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="./images/favicon//favicon-16x16.png">
<link rel="manifest" href="./images/favicon//manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="./images/favicon//ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!--  -->

<!--METATAG SHIT -->
<meta name="description" content="beschrijving die ik nog moet aanvullen en waarschijnlijk ga vergeten">
<meta name="keywords" content="project, thomas more, nss, netwerken systemen en security, rob van keilegom">
<meta name="author" content="Rob Van Keilegom" >
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="1 month">
<!--  -->

		<title>  Home </title>
        <script type="text/javascript">
  <!--
    function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
    function check()
    {
		  var first_name = document.getElementById("first_name");
      	  var last_name = document.getElementById("last_name");
      	  var email = document.getElementById("email");
      	  var comment = document.getElementById("comment");
      
		  if(first_name.value == "" || last_name.value == "" || email.value == "" || comment.value == "" || !validateEmail(email.value))
		  {
				document.getElementById("error").style.display = "block";
              return false;
		  }
            else {
                document.getElementById("error").style.display = "none";
                return true;
            }
      
		  
      
    }
  -->
  </script>
	</head>
<body>
	
<header>
      <p class="header_title">Project battleship</p>
    <p class="header_sub_desktop">Een Project Van 1ste Jaar Studenten Electronica ICT: Netwerken, Systemen &amp; Security - Marlon Stoops &amp; Rob Van Keilegom</p>
  <p class="header_sub_mobile">Een Project Van Marlon Stoops &amp; Rob Van Keilegom (1NSS)</p>
  
<img class="shipicon" src="./images/battleship_icon.png" alt="">
</header>

<nav id="nav" role="navigation">
<a href="#nav" title="Show navigation"><img src="./images/nav_expand.png" alt="Expand Nav"></a>
<a href="#" title="Hide navigation"><img src="./images/nav_expand.png" alt="Expand Nav"></a>
<ul>
<li><a href="home.php" class="link">Home</a></li>
<li><a href="./" class="link">Intro</a></li>
<li><a href="hardware.php" class="link">Hardware</a></li>
<li><a href="arduino.php" class="link">Arduino</a></li>
<li><a href="website.php" class="link">Website</a></li>
<li><a href="about.php" class="link">Over Mij</a></li>
<li><a href="contact.php" class="link">Contact</a></li>
</ul>
</nav>	

<div class="main">

<div class="content">
<h1>Contact</h1>
    <p class="error" id="error">Error: Niet alle velden zijn (correct) ingevuld.</p>
<form name="contact_form" method="post" enctype="text/plain"  action="mailto:r0487008@student.thomasmore.be" onSubmit="return check()">
<table class="contact_form">
</tr>
<tr>
 <td>
  <label for="first_name">Voornaam: </label>
 </td>
 <td>
  <input  type="text" name="first_name" id="first_name" maxlength="50">
 </td>
</tr>
 
<tr>
 <td>
  <label for="last_name">Achternaam: </label>
 </td>
 <td>
  <input  type="text" name="last_name" id="last_name" maxlength="50">
 </td>
</tr>
<tr>
 <td>
  <label for="email">Email adres: </label>
 </td>
 <td>
  <input  type="text" name="email" id="email" maxlength="80">
 </td>
 
</tr>
<tr>
 <td>
  <label for="comment">Mededeling: </label>
 </td>
 <td>
  <textarea  name="comment" id="comment" maxlength="1000" cols="25" rows="6"></textarea>
 </td>
</tr>
<tr>
 <td colspan="2" style="text-align:center">
  <input type="submit" value="Verzenden"> 
 </td>
</tr>
</table>
</form>
<!--
<div id="socialmedia">
<img src="images/socialmedia/email.png" alt=""> Email <br />
<img src="images/socialmedia/email.png" alt=""> Email Thomas More
</div>
-->
</div>
</div>
	
    <footer>
    <p>
Copyright &copy; 2015 | Thomas More Mechelen-Antwerpen vzw |
Campus De Nayer | Professionele Bachelor Elektronica-Ict: Netwerken, Systemen En Security</p>
<img src="images/footer/tm.jpg" alt="tm_footer_img"/>

<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valide CSS!" /></a>

    <a href="http://www.w3.org/html/logo/"><img src="http://www.w3.org/html/logo/badge/html5-badge-h-solo.png" width="63" height="64" alt="HTML5 Powered" title="HTML5 Powered"></a>
        
</footer>

<script>
   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = 220;
			 if ($(window).scrollTop() > navHeight) {
				 $('nav').addClass('fixed');
			 }
			 else {
				 $('nav').removeClass('fixed');
			 }
		});
	});
</script>
</body>
</html>