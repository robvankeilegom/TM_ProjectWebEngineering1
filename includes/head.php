<meta charset="utf-8" />
<meta name="viewport" content="initial-scale=1">
<script src="js/jquery-2.1.4.js"></script>
<script src="js/nav.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>

<!-- stylesheets -->
<link rel="stylesheet" type="text/css" href="./stylesheets/reset.css" />
<link rel="stylesheet" type="text/css" href="./stylesheets/mainstyle.css" />
<link rel="stylesheet" type="text/css" href="./stylesheets/mainstyle_css3.css" />
<link rel="stylesheet" media="screen and (min-device-width: 800px)" type="text/css" href="./stylesheets/desktop.css" />
<link rel="stylesheet" media="screen and (min-device-width: 800px)" type="text/css" href="./stylesheets/desktop_css3.css" />
<link rel="stylesheet" media="screen and (max-device-width: 800px)" href="./stylesheets/mobile.css" />
<link rel="stylesheet" media="screen and (max-device-width: 800px)" href="./stylesheets/mobile_css3.css" />

<!-- fancybox -->
<link rel="stylesheet" href="stylesheets/jquery.fancybox.css" type="text/css" media="screen" />
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css' /> <!-- headfont -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /><!-- navfont -->


<!-- FAVICON SHIT -->

<link rel="apple-touch-icon" sizes="57x57" href="./images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="./images/favicon//apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="./images/favicon//apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="./images/favicon//apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="./images/favicon//apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="./images/favicon//apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="./images/favicon//apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="./images/favicon//apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="./images/favicon//apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="./images/favicon//android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="./images/favicon//favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="./images/favicon//favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="./images/favicon//favicon-16x16.png">
<link rel="manifest" href="./images/favicon//manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="./images/favicon//ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!--  -->

<!--METATAG SHIT -->
<meta name="description" content="beschrijving die ik nog moet aanvullen en waarschijnlijk ga vergeten">
<meta name="keywords" content="project, thomas more, nss, netwerken systemen en security, rob van keilegom">
<meta name="author" content="Rob Van Keilegom" >
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="1 month">
<!--  -->
